const express = require("express");
const app = express();
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const userRoute = require("./routes/user")
const authRoute = require("./routes/auth")
const productRoute = require("./routes/product")
const orderRoute = require("./routes/order")
const cartRoute = require("./routes/cart")

dotenv.config();



mongoose.connect(process.env.MONGODB)
.then(()=>console.log("DB Connection Successful")).catch((err) =>{
    console.log(err);
  });


app.use(express.json());
app.use("/api/auth", authRoute);
app.use("/api/users", userRoute);
app.use("/api/products", productRoute);
app.use("/api/carts", cartRoute);
app.use("/api/orders", orderRoute);

app.get('/', (req, res) => {
    res.send('Welcome to Domz-Grind-Online-Store')
  }); 

let port = process.env.PORT; 

app.listen(port, ()=>{
  console.log(`App is running the port ${port}`);
})